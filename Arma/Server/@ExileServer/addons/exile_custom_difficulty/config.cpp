/*

Exile Custom Difficulty by [FPS]kuplion

*/

class CfgPatches
{
	class exile_custom_difficulty
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"exile_client","exile_server_config"};
	};
};

class CfgFunctions
{
	class exile_custom_difficulty
	{
		class exile_custom_difficulty_init
		{
			file = "exile_custom_difficulty\init";
			class preInit
			{
				preInit = 1;
			};
			class postInit
			{
				postInit = 1;
			};
		};
	};
};

class CfgDifficultyPresets
{
	class Veteran;
	defaultPreset = "ExileRegular";
	class ExileRegular: Veteran
	{
		BleedingRate = 0.0005;
		displayName = "Regular Exile";
		class Options
		{
			reducedDamage = 0;
			groupIndicators = 2;
			friendlyTags = 2;
			enemyTags = 0;
			detectedMines = 2;
			commands = 2;
			waypoints = 2;
			weaponInfo = 2;
			stanceIndicator = 1;
			staminaBar = 0;
			weaponCrosshair = 1;
			visionAid = 0;
			thirdPersonView = 1;
			cameraShake = 1;
			scoreTable = 0;
			deathMessages = 1;
			vonID = 0;
			squadRadar = 0;
			mapContent = 1;
			autoReport = 0;
			multipleSaves = 0;
		};
	};
	class ExileHardcore: Veteran
	{
		BleedingRate = 0.005;
		displayName = "Hardcore Exile";
		class Options
		{
			reducedDamage = 0;
			groupIndicators = 2;
			friendlyTags = 2;
			enemyTags = 0;
			detectedMines = 2;
			commands = 2;
			waypoints = 2;
			weaponInfo = 2;
			stanceIndicator = 1;
			staminaBar = 0;
			weaponCrosshair = 1;
			visionAid = 0;
			thirdPersonView = 1;
			cameraShake = 1;
			scoreTable = 0;
			deathMessages = 1;
			vonID = 0;
			squadRadar = 0;
			mapContent = 1;
			autoReport = 0;
			multipleSaves = 0;
		};
	};
};