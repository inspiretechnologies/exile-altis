/*
 * This file is subject to the terms and conditions defined in
 * file 'APL-SA LICENSE.txt', which is part of this source code package.
 */

BS_debug_logCrateFill = true; // True to log items spawned in crates to server .RPT, usually right after [Display #24]

BS_player_showCrateClaimMessage = false; // True to show toast and chat notification with coordinates to all players when any players are close to crate
BS_player_showCrateClaimMessageRadius = 20; // Players must be this close (in meters) to trigger serverwide chat/toast notification

BS_class_crate = "Exile_Container_SupplyBox"; // Class of loot crate.
BS_class_wreckage = "Land_UWreck_FishingBoat_F"; // Class of shipwreck.

BS_count_shipwrecks = 10; // Total wrecks

BS_locations_crateWreckOffset = 12; // Distance from wreck to spawn crate.
BS_locations_center = [14912.4,15108.7,0]; // Center o fmap from which to spawn wrecks, on Altis this is in the central bay
BS_locations_distance_min = 400; // Minimum distance from BS_location_center to spawn crate.
BS_locations_distance_max = 10000; // Maximum distance from BS_locations_center to spawn crate. Careful putting this too high or they will spawn off the map!

BS_loot_enablePoptabs = true; // True to spawn random number of poptabs in crates, otherwise false.
BS_loot_count_poptabs_seed = [3000, 10000, 24000]; // min/mid/max, so will spawn around 5k most of the time with small chance to be much closer to 18k and small chance to be closer to 3k

BS_loot_itemCargo = // Items to put in loot crate.
[   // [class (if array, picks one random item), guaranteed amount, possible random additional amount, % chance of spawning additional random amount]
    ["Exile_Item_SafeKit", 0, 1, 100], // 100% of the time 0-1 safes will spawn.
    ["Exile_Weapon_AKS_Gold", 0, 2, 100], // 100% of the time 0-2 safes will spawn.
    ["Exile_Magazine_30Rnd_762x39_AK", 0, 2, 100],
    ["Exile_Weapon_TaurusGold", 1, 1, 100], // One pistol guaranteed, with 100% chance to spawn random pistol (so really 50%)
    ["Exile_Magazine_6Rnd_45ACP", 0, 3, 100],
    ["arifle_SDAR_F", 1, 1, 50],
    ["20Rnd_556x45_UW_mag", 1, 2, 100], // One mag guaranteed, with 100% chance to spawn between 0-2 more mags 
    ["SatchelCharge_Remote_Mag", 1, 2, 100],
    ["Exile_Item_Defibrillator", 1, 2, 100],
    ["Exile_Item_Rope", 1, 1, 100],
    ["Exile_Item_Vishpirin", 1, 3, 100],
    ["Exile_Item_DuctTape", 1, 2, 100],
    ["Exile_Item_PlasticBottleFreshWater", 0, 2, 100],
    ["Exile_Item_EMRE", 0, 2, 100],
    [["V_RebreatherB", "V_RebreatherIA", "V_RebreatherIR"], 1, 1, 100],
    [["G_Diving", "G_B_Diving", "G_O_Diving", "G_I_Diving"], 1, 1, 100],
    [["NVGoggles", "NVGoggles_INDEP", "NVGoggles_OPFOR"], 1, 2, 100],
    ["Exile_Item_ConcreteWallKit", 0, 1, 100],
    ["Exile_Item_ConcreteFloorKit", 0, 1, 100],
    ["Exile_Item_FortificationUpgrade", 0, 2, 100],
    ["Exile_Item_RubberDuck", 0, 2, 14], // No ducks guaranteed, but 14% of the time, an additional 0-2 ducks could spawn.
    ["Exile_Item_Knife", 0, 1, 25], // No knives guaranteed, but 25% of the time an additional 0-1 knives could spawn.
	["Land_Metal_wooden_rack_F_Kit",0,1,2],
	["Land_Sign_WarningMilitaryArea_F_Kit",0,1,2],
	["Land_Wall_Tin_4_Kit",0,1,2],
	["Land_ShelvesMetal_F_Kit",0,1,2],
	["Land_cargo_house_slum_F_Kit",0,1,2],
	["Land_Cargo_House_V2_F_Kit",0,1,2],
	["Land_LampHalogen_F_Kit",0,1,2],
	["Land_Shed_Small_F_Kit",0,1,2],
	["Land_Razorwire_F_Kit",0,1,2],
	["Land_Sign_WarningMilitaryVehicles_F_Kit",0,1,2],
	["Land_Sign_WarningMilAreaSmall_F_Kit",0,1,2],
	["Land_Concrete_SmallWall_8m_F_Kit",0,1,2],
	["Land_Concrete_SmallWall_4m_F_Kit",0,1,2],
	["Land_PortableLight_double_F_Kit",0,1,2],
	["Land_TableDesk_F_Kit",0,1,2],
	["Land_ToiletBox_F_Kit",0,1,2],
	["Land_ChairWood_F_Kit",0,1,2],
	["Land_Sun_chair_F_Kit",0,1,2],
	["Land_Sunshade_04_F_Kit",0,1,2],
	["Land_LampShabby_F_Kit",0,1,2],
	["Land_Sign_WarningUnexplodedAmmo_F_Kit",0,1,2],
	["Exile_Plant_GreenBush_Kit",0,1,2],
	["Land_SharpStone_01_F_Kit",0,1,2],
	["Land_SharpStone_02_F_Kit",0,1,2],
	["Land_Sleeping_bag_F_Kit",0,1,2],
	["Land_Small_Stone_02_F_Kit",0,1,2],
	["Land_SolarPanel_2_F_Kit",0,1,2],
	["Land_CampingChair_V2_F_Kit",0,1,2],
	["Land_CampingChair_V1_F_Kit",0,1,2],
	["Land_Camping_Light_F_Kit",0,1,2],
	["Land_CampingTable_F_Kit",0,1,2],
	["MapBoard_altis_F_Kit",0,1,2],
	["Exile_ConcreteMixer_Kit",0,1,2],
	["Land_Metal_rack_F_Kit",0,1,2],
	["Land_Sink_F_Kit",0,1,2],
	["Land_i_House_Small_03_V1_F_Kit",0,1,2],
	["Land_FlatTV_01_F_Kit",0,1,2],
	["Land_ChairPlastic_F_Kit",0,1,2],
	["Land_GamingSet_01_console_F_Kit",0,1,2],
	["Land_GamingSet_01_controller_F_Kit",0,1,2],
	["Land_GymBench_01_F_Kit",0,1,2],
	["Land_GymRack_03_F_Kit",0,1,2],
	["Land_OfficeCabinet_01_F_Kit",0,1,2],
	["Land_OfficeChair_01_F_Kit",0,1,2],
	["Land_PCSet_01_case_F_Kit",0,1,2],
	["Land_PCSet_01_keyboard_F_Kit",0,1,2],
	["Land_PCSet_01_mouse_F_Kit",0,1,2],
	["Land_PCSet_01_screen_F_Kit",0,1,2],
	["Land_Printer_01_F_Kit",0,1,2],
	["Land_RattanChair_01_F_Kit",0,1,2],
	["Land_RattanTable_01_F_Kit",0,1,2],
	["Land_Sleeping_bag_blue_F_Kit",0,1,2],
	["Land_Sleeping_bag_brown_F_Kit",0,1,2],
	["Land_Trophy_01_bronze_F_Kit",0,1,2],
	["Land_Trophy_01_gold_F_Kit",0,1,2],
	["Land_Trophy_01_silver_F_Kit",0,1,2],
	["Land_Sun_chair_green_F_Kit",0,1,2],
	["Land_Sunshade_01_F_Kit",0,1,2],
	["Land_Sunshade_02_F_Kit",0,1,2],
	["Land_Sunshade_03_F_Kit",0,1,2],
	["Land_Sunshade_F_Kit",0,1,2],
	["Land_TablePlastic_01_F_Kit",0,1,2],
	["Land_WoodenTable_large_F_Kit",0,1,2],
	["Land_WoodenTable_small_F_Kit",0,1,2],
	["OfficeTable_01_new_F_Kit",0,1,2],
	["Land_EngineCrane_01_F_Kit",0,1,2],
	["Land_PalletTrolley_01_yellow_F_Kit",0,1,2],
	["Land_PressureWasher_01_F_Kit",0,1,2],
	["Land_WeldingTrolley_01_F_Kit",0,1,2],
	["Land_Workbench_01_F_Kit",0,1,2],
	["ArrowDesk_L_F_Kit",0,1,2],
	["ArrowDesk_R_F_Kit",0,1,2],
	["PlasticBarrier_02_grey_F_Kit",0,1,2],
	["PlasticBarrier_02_yellow_F_Kit",0,1,2],
	["PlasticBarrier_03_blue_F_Kit",0,1,2],
	["PlasticBarrier_03_orange_F_Kit",0,1,2],
	["TapeSign_F_Kit",0,1,2],
	["Fridge_01_closed_F_Kit",0,1,2],
	["Land_MetalCase_01_large_F_Kit",0,1,2],
	["Land_Microwave_01_F_Kit",0,1,2],
	["Land_ShelvesWooden_F_Kit",0,1,2],
	["Land_ShelvesWooden_blue_F_Kit",0,1,2],
	["Land_ShelvesWooden_khaki_F_Kit",0,1,2],
	["Land_ToolTrolley_01_F_Kit",0,1,2],
	["Land_ToolTrolley_02_F_Kit",0,1,2],
	["Land_Sign_Mines_F_Kit",0,1,2],
	["Land_GamingSet_01_powerSupply_F_Kit",0,1,2],
	["Land_GamingSet_01_camera_F_Kit",0,1,2],
	["Land_TripodScreen_01_dual_v2_F_Kit",0,1,2],
	["Land_TripodScreen_01_dual_v1_F_Kit",0,1,2],
	["Land_SatelliteAntenna_01_F_Kit",0,1,2],
	["Land_Projector_01_F_Kit",0,1,2],
	["MetalBarrel_burning_F_Kit",0,1,2],
	["EBM_Brickwall_window_Kit",0,1,2],
	["EBM_Brickwall_stairs_Kit",0,1,2],
	["EBM_Brickwall_floorport_door_Kit",0,1,2],
	["EBM_Brickwall_floorport_Kit",0,1,2],
	["EBM_Brickwall_floor_Kit",0,1,2],
	["EBM_Brickwall_door_Kit",0,1,2],
	["EBM_Brickwall_hole_Kit",0,1,2],
	["EBM_Helipad_Kit",0,1,2],
	["EBM_Airhook_Kit",0,1,2],
	["EBM_Parksign_Kit",0,1,2],
	["EBM_Brickwall_Kit",0,1,2],
	["EBM_Medikit_Kit",0,1,2],
	["EBM_pollard_Kit",0,1,2],
	["EBM_ATM_Kit",0,1,2]
]; 

publicVariable "BS_debug_logCrateFill";
publicVariable "BS_player_showCrateClaimMessage";
publicVariable "BS_player_showCrateClaimMessageRadius";
publicVariable "BS_class_crate";
publicVariable "BS_class_wreckage";
publicVariable "BS_count_shipwrecks";
publicVariable "BS_locations_crateWreckOffset";
publicVariable "BS_locations_center";
publicVariable "BS_locations_distance_min";
publicVariable "BS_locations_distance_max";
publicVariable "BS_loot_enablePoptabs";
publicVariable "BS_loot_count_poptabs_seed";
publicVariable "BS_loot_itemCargo";