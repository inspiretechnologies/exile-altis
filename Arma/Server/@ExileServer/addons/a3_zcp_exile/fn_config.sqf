/*
	Zupa's Capture Points
	Configuration of ZCP
	Capture points and earn rewards.

	╔════╗─────────╔═══╗────────╔╗─────────────╔╗
	╚══╗═║─────────╚╗╔╗║────────║║────────────╔╝╚╗
	──╔╝╔╬╗╔╦══╦══╗─║║║╠══╦╗╔╦══╣║╔══╦╗╔╦══╦═╗╚╗╔╬══╗
	─╔╝╔╝║║║║╔╗║╔╗║─║║║║║═╣╚╝║║═╣║║╔╗║╚╝║║═╣╔╗╗║║║══╣
	╔╝═╚═╣╚╝║╚╝║╔╗║╔╝╚╝║║═╬╗╔╣║═╣╚╣╚╝║║║║║═╣║║║║╚╬══║
	╚════╩══╣╔═╩╝╚╝╚═══╩══╝╚╝╚══╩═╩══╩╩╩╩══╩╝╚╝╚═╩══╝
	────────║║
	────────╚╝
*/

// First person in the Cap zone is the capper (If he leaves, the closest on of the group is the new capper but time is reset!).
// When multiple people are in the zone and not in the same group, the zone is contested and the timer pauses
// Being first in the zone starts the timer.
// Holding a zone  gives you a reward after x Min.

ZCP_dev = false; // Devmode for shorter development capture times

ZCP_AI_Type = 'DMS'; // NONE | DMS | FUMS

ZCP_useOldMessages = false;

/*Exile Toasts Notification Settings*/
ZCP_DMS_ExileToasts_Title_Size			= 22;					// Size for Client Exile Toasts  mission titles.
ZCP_DMS_ExileToasts_Title_Font			= "puristaMedium";			// Font for Client Exile Toasts  mission titles.
ZCP_DMS_ExileToasts_Message_Color		= "#FFFFFF";				// Exile Toasts color for "ExileToast" client notification type.
ZCP_DMS_ExileToasts_Message_Size		= 19;					// Exile Toasts size for "ExileToast" client notification type.
ZCP_DMS_ExileToasts_Message_Font		= "PuristaLight";			// Exile Toasts font for "ExileToast" client notification type.
/*Exile Toasts Notification Settings*/

ZCP_AI_useLaunchersChance = 100; // %Change to spawn Launcher on AI soldier ( never exceeds the MIN and MAX defined per cappoint).

// Put the following to -1 to disable it.
ZCP_AI_killAIAfterMissionCompletionTimer = 120; // Amount of seconds before all ZCP AI get auto killed after a mission is completed. ( DMS only ).

// ZCP_Min_AI_Amount = 4; Not used anymore
// ZCP_Random_AI_Max = 8; Not used anymore

ZCP_MessagePlayersBeforeWaves = true; // True -> Inform for an icoming wave of AI, false is not inform the players inside.

// ZCP_CapTime = 300; // Now defined for each mission seperate
ZCP_ServerStartWaitTime = 120;
ZCP_MinWaitTime = 600; // seconds to wait to spawn a new capturepoint when 1 was capped.
ZCP_MaxWaitTime = 120; // random between 0 and THIS number added to the ZCP_MinWaitTime to counter spawning points at the same time
ZCP_BaseCleanupDelay = 300; // seconds to wait to delete a captured base.

ZCP_RewardRelativeToPlayersOnline = true; // This will recalculate the crypto reward according the amount of online players.
ZCP_PoptabReward = 10000; // Poptab reward for capping per player online. ( When poptab reward is selected or randomly chosen ).
ZCP_MinPoptabReward = 25000; // Poptabreward is added to this number

ZCP_ReputationReward = 1000; // Respect reward for capping per  player online.
ZCP_MinReputationReward = 1000; // ZCP_ReputationReward is added to this number
ZCP_ReputationRewardForGroup = 1000; // Each group members gets this amount of reputation ( for the trouble).
ZCP_CONFIG_GroupDistanceForRespect = 200; // meters to be close to the capper to get the group award

ZCP_CleanupBase = true; // Let the base dissappear after completing
ZCP_CleanupBaseWithAIBomber = true; // Cleanup with a airstrike
ZCP_CleanupAIVehicleClasses = ['B_Plane_CAS_01_F']; // Any flying vehicle in arma (default B_Plane_CAS_01_F = A10)
ZCP_FlyHeight = 50; // Height of the flying plane;

ZCP_BomberCanDestroyMapBuildings = true; // if true damage of the bombs are applied to all objects in the blastradius.
// If false nothing gets hit ( except players and vehicles ) - base cleanup will happen in both cases after explosion.

ZCP_UseSpecificNamesForCappers = true; // Use the player name, if false it says 'A player'

// ZCP_giveSurvivalBoxWithPoptabsReward = true; not used anymore. You can now define multiple rewards per mission.
ZCP_RewardWeightForRandomChoice = [
	["Poptabs", 4],
	["BuildBox", 3],
	["WeaponBox", 4],
	["SurvivalBox", 4],
	["Vehicle", 2],
	["SniperWeaponBox", 1],
	["BigWeaponBox", 2]
];
// How does this work ( 6 + 3 + 5 + 2 = 16)
// 6/16 = 37.50 %
// 3/16 = 18.75 %
// 5/16 = 31.25 %
// 2/16 = 12.50 %
// You can add extra types here if you want them in the random option.

// Server will keep as many missions up as ZCP_MaxMissions, And they will be randomly chosen from the following list

ZCP_MaxMissions = 2; // Amount of cap points at the same time when ZCP_MaxMissionsRelativeToPlayers = false

ZCP_Minimum_Online_Players = 3; // Amount of players to be online before it allows to spawn a capture point. !!! O = always

ZCP_MaxMissionsRelativeToPlayers = true; // ZCP_MaxMissions will be ignored if true. ZCP_RelativeMaxMissions will be used
ZCP_RelativeMaxMissions = [
    //[ min players,  amount of cappoints],
    [5, 1],
    [15, 2],
    [40, 3],
    [65, 4]
];
ZCP_SecondsCheckPlayers = 600; // seconds for loop check if the server holds more players now (and spawn extra cappoints). ( 600 = every 10 minuts)

// For every spawned mission,
// buildeditor currenty supported -> m3e, xcam, EdenConverted ( THis is exported as terrainbuilder and converted with my site), m3eEden
ZCP_CapBases = [ // located in capbases folder [filename, capradius, buildeditor, max terraingradient (if not overwritten by staticbasefile), radius of open space for it to spawn base]
	["m3e_village.sqf", 100, "m3e", 90, 50],
	["xcam_milPoint.sqf", 200, "xcam", 90, 50],
	["ec_audacity.sqf", 200, "EdenConverted", 90, 30],
	["ec_bravery.sqf", 200, "EdenConverted", 90, 35],
	["ec_courage.sqf", 200, "EdenConverted", 90, 25],
	["ec_defiance.sqf", 200, "EdenConverted", 90, 20],
	["ec_endurance.sqf", 200, "EdenConverted", 90, 20],
	["ec_fortitude.sqf", 200, "EdenConverted", 90, 25],
	["m3e_exoBase1.sqf", 200, "m3e", 90, 50],
	["m3e_exoBase2.sqf", 200, "m3e", 90, 50],
	["m3e_exoBase3.sqf", 200, "m3e", 90, 50]
];

ZCP_Blacklist = [ // [ [x,y,z], radius ];
	[[23644,18397,0] , 1200], // altis saltlake
	[[-999,-999,0] , 500],
	[[-999,-999,0] , 500]
];

ZCP_createVirtualCircle = true;

ZCP_circleNeutralColor = "#(rgb,8,8,3)color(0,1,0,1)"; // green
ZCP_circleCappingColor = "#(rgb,8,8,3)color(0,0,1,1)"; // blue
ZCP_circleContestedColor = "#(rgb,8,8,3)color(1,0,0,1)"; // red

//Boxtypes
ZCP_SurvivalBox = "Exile_Container_SupplyBox";
ZCP_BuildingBox = "Exile_Container_SupplyBox";
ZCP_WeaponBox = "Exile_Container_SupplyBox";

/* 3.1 new configs */
ZCP_CONFIG_TerritoryDistance = 0;  // Distance from territories. ( 0 to disable )

ZCP_CONFIG_AI_side = east; // The side where the AI is on.
ZCP_CONFIG_AI_soldierClass = 'O_G_Soldier_F'; // The class model for the soldier ( This needs to be a soldier from the AI faction! -> otherwise they shoot eachother on spawn)
ZCP_CONFIG_MaxRandomAIMoney = 250; // Max poptabs on in AI it's inventory. ( Random between 0 -> this number ).

// These are used when the cappoint is a city point.
ZCP_CONFIG_UseCityName = true; // Use City name CP for maker naming instead of ZCP alpha..
ZCP_CONFIG_CityDistanceToPlayer = 100; // distance for the town to be from a player ( From center town )
ZCP_CONFIG_CityDistanceToTrader = 2500; // distance for the town to be from a trader ( From center town )
ZCP_CONFIG_CityDistanceToSpawn = 500; // distance for the town to be from a spawnpoint ( From center town )
ZCP_CONFIG_CityDistanceToTerritory = 100; // distance for the town to be from a spawnpoint ( From center town )
ZCP_CONFIG_CityDistanceToAI = 100; // distance for the town to be from other AI missions, patrols ..

/* END NEW CONFIGS 3.1 */

// Same as DMS -> Credits DMS
ZCP_DistanceBetweenMissions = 500;
ZCP_SpawnZoneDistance = 500;
ZCP_TradeZoneDistance = 2500;
ZCP_DistanceFromWater = 100;
ZCP_DistanceFromPlayers = 200;
ZCP_DistanceFromBaseObjects = 100;

ZCP_CONFIG_BaseObjectsClasses = [
                                    'Exile_Construction_Abstract_Physics',
                                     'Exile_Construction_Abstract_Static'
                                 ];


ZCP_TraderZoneMarkerTypes =			[							// If you're using custom trader markers, make sure you define them here. CASE SENSITIVE!!!
										"ExileTraderZone"
									];
ZCP_SpawnZoneMarkerTypes =			[							// If you're using custom spawn zone markers, make sure you define them here. CASE SENSITIVE!!!
										"ExileSpawnZone"
									];



/* These are arma 3 colors, look up the color naming if you are going to change this */
ZCP_FreeColor = "ColorGreen"; // uncontested marker color -> also correct size
ZCP_CappedColor = "ColorBlue"; // uncontested + capping color
ZCP_ContestColor = "ColorRed"; // contested + capping color
ZCP_BackgroundColor = "ColorWhite"; // Color to get attention on the map, if zoomed out this will be bigger then the cap circle which is the normal size.
ZCP_MissionMarkerWinDotTime = 600; // Seconds to show a marker after a capped point. Change to 0 to disable!

ZCP_DisableVehicleReward = false; // Because it doesnt save without changing epoch code.

/* Uses DMS system, why make one if it already excist? Credits DMS */
ZCP_DMS_MinimumMagCount					= 2;						// Minimum number of magazines for weapons.
ZCP_DMS_MaximumMagCount					= 4;						// Maximum number of magazines for weapons.
ZCP_DMS_CrateCase_Sniper =				[							// If you pass "Sniper" in _lootValues, then it will spawn these weapons/items/backpacks
                                            [
                                                ["Rangefinder",1],
                                                ["srifle_GM6_F",1],
                                                ["srifle_LRR_F",1],
                                                ["srifle_EBR_F",1],
                                                ["hgun_Pistol_heavy_01_F",1],
                                                ["hgun_PDW2000_F",1]
                                            ],
                                            [
                                                ["ItemGPS",1],
                                                ["U_B_FullGhillie_ard",1],
                                                ["U_I_FullGhillie_lsh",1],
                                                ["U_O_FullGhillie_sard",1],
                                                ["U_O_GhillieSuit",1],
                                                ["V_PlateCarrierGL_blk",1],
                                                ["V_HarnessO_brn",1],
                                                ["Exile_Item_InstaDoc",3],
                                                ["Exile_Item_Surstromming_Cooked",5],
                                                ["Exile_Item_PlasticBottleFreshWater",5],
                                                ["optic_DMS",1],
                                                ["acc_pointer_IR",1],
                                                ["muzzle_snds_B",1],
                                                ["optic_LRPS",1],
                                                ["optic_MRD",1],
                                                ["muzzle_snds_acp",1],
                                                ["optic_Holosight_smg",1],
                                                ["muzzle_snds_L",1],
                                                ["5Rnd_127x108_APDS_Mag",3],
                                                ["7Rnd_408_Mag",3],
                                                ["20Rnd_762x51_Mag",5],
                                                ["11Rnd_45ACP_Mag",3],
                                                ["30Rnd_9x21_Mag",3]
                                            ],
                                            [
                                                ["B_Carryall_cbr",1],
                                                ["B_Kitbag_mcamo",1]
                                            ]
                                        ];
ZCP_DMS_BoxWeapons =					[							// List of weapons that can spawn in a crate
										"Exile_Melee_Axe",
										"arifle_Katiba_GL_F",
										"arifle_MX_GL_Black_F",
										"arifle_Mk20_GL_F",
										"arifle_TRG21_GL_F",
										"arifle_Katiba_F",
										"arifle_MX_Black_F",
										"arifle_TRG21_F",
										"arifle_TRG20_F",
										"arifle_Mk20_plain_F",
										"arifle_Mk20_F",
										"LMG_Zafir_F",
										"LMG_Mk200_F",
										"arifle_MX_SW_Black_F",
										"srifle_EBR_F",
										"srifle_DMR_01_F",
										"srifle_GM6_F",
										"srifle_LRR_F",
										"arifle_MXM_F",
										"arifle_MXM_Black_F",
										"srifle_DMR_02_F",
										"HAP_CHEY_camo2_blue",
										"HAP_CHEY_camo2_woodland",
										"HAP_CHEY_camo3_black",
										"HAP_CHEY_camo3_green",
										"HAP_CHEY_camo3_pink",
										"HAP_CHEY_leo1",
										"HAP_CHEY_teal",
										"HAP_CHEY_white",
										"HAP_CHEY_woodland1",
										"HAP_CHEY_woodland1_green",
										"HAP_CHEY_woodland1_orange",
										"HAP_DMR_black",
										"HAP_DMR_camoORN",
										"HAP_DMR_digiGRN",
										"HAP_DMR_leo",
										"HAP_DMR_leoA",
										"HAP_DMR_snow",
										"HAP_DMR_zebra",
										"HAP_EBR_black",
										"HAP_EBR_desert",
										"HAP_EBR_desertA",
										"HAP_EBR_snow",
										"HAP_EBR_wavesblu",
										"HAP_LYNX_camo1_blue",
										"HAP_LYNX_camo1_green",
										"HAP_LYNX_camo1_tan",
										"HAP_LYNX_green",
										"HAP_LYNX_hex_beige",
										"HAP_LYNX_hex_blue",
										"HAP_LYNX_orange",
										"HAP_LYNX_red",
										"HAP_LYNX_white",
										"HAP_LYNX_yellow",
										"HAP_MARKS_ASP_blue",
										"HAP_MARKS_ASP_blueCamo",
										"HAP_MARKS_ASP_orangeCamo",
										"HAP_MARKS_ASP_snow",
										"HAP_MARKS_ASP_unicornPuke",
										"HAP_MARKS_ASP_white",
										"HAP_MARKS_CYRUS_brightRed",
										"HAP_MARKS_CYRUS_camo1",
										"HAP_MARKS_CYRUS_camo2",
										"HAP_MARKS_CYRUS_eyesOn1",
										"HAP_MARKS_CYRUS_herb1",
										"HAP_MARKS_CYRUS_herb2",
										"HAP_MARKS_CYRUS_herb3",
										"HAP_MARKS_CYRUS_limeGreen",
										"HAP_MARKS_CYRUS_orange1",
										"HAP_MARKS_CYRUS_red1",
										"HAP_MARKS_CYRUS_skulls1",
										"HAP_MARKS_CYRUS_skullsRed1",
										"HAP_MARKS_CYRUS_zebra1",
										"HAP_MARKS_DMR_02_camo1",
										"HAP_MARKS_DMR_02_camo2",
										"HAP_MARKS_DMR_02_camo3",
										"HAP_MARKS_DMR_02_herb1",
										"HAP_MARKS_DMR_02_herb2",
										"HAP_MARKS_DMR_02_herb3",
										"HAP_MARKS_DMR_02_rainbowsunshine1",
										"HAP_MARKS_DMR_02_squared1",
										"HAP_MARKS_EMR_camo1",
										"HAP_MARKS_EMR_camo2",
										"HAP_MARKS_EMR_purple1",
										"HAP_MARKS_EMR_rainbow1",
										"HAP_MARKS_EMR_red1",
										"HAP_MARKS_EMR_teal1",
										"HAP_MARKS_EMR_white1",
										"HAP_MARKS_EMR_whiteCamo1",
										"HAP_MARKS_M14_camo1",
										"HAP_MARKS_M14_camo2",
										"HAP_MARKS_M14_camo3",
										"HAP_MARKS_M14_flames1",
										"HAP_MARKS_M14_white",
										"HAP_MARKS_M14_wood1",
										"HAP_MX_camo1_green",
										"HAP_MX_camo1_purple",
										"HAP_MX_camo1_yellow",
										"HAP_MX_green",
										"HAP_MX_hex_beige",
										"HAP_MX_hex_blue",
										"HAP_MX_hex_green",
										"HAP_MX_pink",
										"HAP_MX_red",
										"HAP_MX_teal",
										"HAP_MX_yellow",
										"CUP_srifle_AWM_des",
										"CUP_srifle_AWM_wdl",
										"CUP_srifle_CZ750",
										"CUP_srifle_DMR",
										"CUP_srifle_CZ550",
										"CUP_srifle_LeeEnfield",
										"CUP_srifle_M14",
										"CUP_srifle_Mk12SPR",
										"CUP_srifle_M24_des",
										"CUP_srifle_M24_wdl",
										"CUP_srifle_M40A3",
										"CUP_srifle_M107_Base",
										"CUP_srifle_M110",
										"CUP_srifle_SVD",
										"CUP_srifle_SVD_des",
										"CUP_srifle_ksvk",
										"CUP_srifle_VSSVintorez",
										"CUP_srifle_AS50",
										"CUP_arifle_AK74",
										"CUP_arifle_AK107",
										"CUP_arifle_AK107_GL",
										"CUP_arifle_AKS74",
										"CUP_arifle_AKS74U",
										"CUP_arifle_AK74_GL",
										"CUP_arifle_AKM",
										"CUP_arifle_AKS",
										"CUP_arifle_AKS_Gold",
										"CUP_arifle_RPK74",
										"CUP_arifle_CZ805_A2",
										"CUP_arifle_FNFAL",
										"CUP_arifle_FNFAL_railed",
										"CUP_arifle_G36A",
										"CUP_arifle_G36A_camo",
										"CUP_arifle_G36K",
										"CUP_arifle_G36K_camo",
										"CUP_arifle_G36C",
										"CUP_arifle_G36C_camo",
										"CUP_arifle_MG36",
										"CUP_arifle_MG36_camo",
										"CUP_arifle_L85A2",
										"CUP_arifle_L85A2_GL",
										"CUP_arifle_L86A2",
										"CUP_arifle_M16A2",
										"CUP_arifle_M16A2_GL",
										"CUP_arifle_M16A4_GL",
										"CUP_arifle_M4A1",
										"CUP_arifle_M4A1_camo",
										"CUP_arifle_M16A4_Base",
										"CUP_arifle_M4A1_BUIS_GL",
										"CUP_arifle_M4A1_BUIS_camo_GL",
										"CUP_arifle_M4A1_BUIS_desert_GL",
										"CUP_arifle_M4A1_black",
										"CUP_arifle_M4A1_desert",
										"CUP_arifle_Sa58P",
										"CUP_arifle_Sa58V",
										"CUP_arifle_Mk16_CQC",
										"CUP_arifle_XM8_Compact_Rail",
										"CUP_arifle_XM8_Railed",
										"CUP_arifle_XM8_Carbine",
										"CUP_arifle_XM8_Carbine_FG",
										"CUP_arifle_XM8_Carbine_GL",
										"CUP_arifle_XM8_Compact",
										"CUP_arifle_xm8_SAW",
										"CUP_arifle_xm8_sharpshooter",
										"CUP_arifle_CZ805_A1",
										"CUP_arifle_CZ805_GL",
										"CUP_arifle_CZ805_B_GL",
										"CUP_arifle_CZ805_B",
										"CUP_arifle_Sa58P_des",
										"CUP_arifle_Sa58V_camo",
										"CUP_arifle_Sa58RIS1",
										"CUP_arifle_Sa58RIS2",
										"CUP_arifle_Sa58RIS2_camo",
										"CUP_arifle_Mk16_CQC_FG",
										"CUP_arifle_Mk16_CQC_SFG",
										"CUP_arifle_Mk16_CQC_EGLM",
										"CUP_arifle_Mk16_STD",
										"CUP_arifle_Mk16_STD_FG",
										"CUP_arifle_Mk16_STD_SFG",
										"CUP_arifle_Mk16_STD_EGLM",
										"CUP_arifle_Mk16_SV",
										"CUP_arifle_Mk17_CQC",
										"CUP_arifle_Mk17_CQC_FG",
										"CUP_arifle_Mk17_CQC_SFG",
										"CUP_arifle_Mk17_CQC_EGLM",
										"CUP_arifle_Mk17_STD",
										"CUP_arifle_Mk17_STD_FG",
										"CUP_arifle_Mk17_STD_SFG",
										"CUP_arifle_Mk17_STD_EGLM",
										"CUP_arifle_Mk20",
										"CUP_sgun_AA12",
										"CUP_sgun_M1014",
										"CUP_sgun_Saiga12K",
										"CUP_lmg_L7A2",
										"CUP_lmg_L110A1",
										"CUP_lmg_M240",
										"CUP_lmg_M249",
										"CUP_lmg_M249_para",
										"CUP_lmg_Mk48_des",
										"CUP_lmg_Mk48_wdl",
										"CUP_lmg_PKM",
										"CUP_lmg_UK59",
										"CUP_lmg_Pecheneg",
										"CUP_smg_bizon",
										"CUP_smg_EVO",
										"CUP_smg_MP5SD6",
										"CUP_smg_MP5A5",
										"rhs_weap_m249_pip_L",
										"rhs_weap_m249_pip_L_para",
										"rhs_weap_m249_pip_L_vfg",
										"rhs_weap_m249_pip_S",
										"rhs_weap_m249_pip_S_para",
										"rhs_weap_m249_pip_S_vfg",
										"rhs_weap_m240B",
										"rhs_weap_m240B_CAP",
										"rhs_weap_m240G",
										"rhs_weap_pkm",
										"rhs_weap_pkp",
										"rhs_weap_m27iar",
										"rhsusf_weap_MP7A2",
										"rhsusf_weap_MP7A2_desert",
										"rhsusf_weap_MP7A2_aor1",
										"rhsusf_weap_MP7A2_winter",
										"rhs_weap_hk416d10",
										"rhs_weap_hk416d10_LMT",
										"rhs_weap_hk416d10_m320",
										"rhs_weap_hk416d145",
										"rhs_weap_hk416d145_m320",
										"rhs_weap_m16a4",
										"rhs_weap_m16a4_carryhandle",
										"rhs_weap_m16a4_carryhandle_M203",
										"rhs_weap_m16a4_carryhandle_pmag",
										"rhs_weap_m4_carryhandle",
										"rhs_weap_m4_carryhandle_pmag",
										"rhs_weap_m4_m203",
										"rhs_weap_m4_m320",
										"rhs_weap_m4a1",
										"rhs_weap_m4a1_blockII",
										"rhs_weap_m4a1_blockII_KAC",
										"rhs_weap_m4a1_blockII_KAC_bk",
										"rhs_weap_m4a1_blockII_KAC_d",
										"rhs_weap_m4a1_blockII_KAC_wd",
										"rhs_weap_m4a1_blockII_M203",
										"rhs_weap_m4a1_blockII_M203_bk",
										"rhs_weap_m4a1_blockII_M203_d",
										"rhs_weap_m4a1_blockII_M203_wd",
										"rhs_weap_m4a1_blockII_bk",
										"rhs_weap_m4a1_blockII_d",
										"rhs_weap_m4a1_blockII_wd",
										"rhs_weap_m4a1_carryhandle",
										"rhs_weap_m4a1_carryhandle_m203",
										"rhs_weap_m4a1_carryhandle_pmag",
										"rhs_weap_m4a1_m203",
										"rhs_weap_m4a1_m320",
										"rhs_weap_mk18",
										"rhs_weap_mk18",
										"rhs_weap_mk18_KAC",
										"rhs_weap_mk18_KAC_bk",
										"rhs_weap_mk18_KAC_d",
										"rhs_weap_mk18_KAC_wd",
										"rhs_weap_mk18_bk",
										"rhs_weap_mk18_d",
										"rhs_weap_mk18_m320",
										"rhs_weap_mk18_wd",
										"rhs_weap_sr25",
										"rhs_weap_sr25_ec",
										"rhs_weap_m14ebrri",
										"rhs_weap_XM2010",
										"rhs_weap_XM2010_wd",
										"rhs_weap_XM2010_d",
										"rhs_weap_XM2010_sa",
										"rhs_weap_svd",
										"rhs_weap_svdp_wd",
										"rhs_weap_svdp_wd_npz",
										"rhs_weap_svdp_npz",
										"rhs_weap_svds",
										"rhs_weap_svds_npz",
										"rhs_weap_M107",
										"rhs_weap_M107_d",
										"rhs_weap_M107_w",
										"rhs_weap_t5000",
										"rhs_weap_m24sws",
										"rhs_weap_m24sws_blk",
										"rhs_weap_m24sws_ghillie",
										"rhs_weap_m40a5",
										"rhs_weap_m40a5_d",
										"rhs_weap_m40a5_w",
										"rhs_weap_sr25_d",
										"rhs_weap_sr25_wd",
										"rhs_weap_sr25_ec_d",
										"rhs_weap_sr25_ec_wd",
										"rhs_weap_kar98k",
										"rhs_weap_m21a",
										"rhs_weap_m21a_fold",
										"rhs_weap_m21a_pr",
										"rhs_weap_m21s",
										"rhs_weap_m21s_fold",
										"rhs_weap_m21s_pr",
										"rhs_weap_m38",
										"rhs_weap_m70ab2",
										"rhs_weap_m70ab2_fold",
										"rhs_weap_m70b1",
										"rhs_weap_m76",
										"rhs_weap_m92",
										"rhs_weap_m92_fold",
										"rhs_weap_m70ab2_fold",
										"rhs_weap_m70b1",
										"rhs_weap_m70b1n",
										"rhs_weap_m70b3n",
										"rhs_weap_m70b3n_pbg40",
										"rhs_weap_m92",
										"rhs_weap_m92_fold",
										"rhs_weap_m76",
										"rhs_weap_m21a",
										"rhs_weap_m21a_pr",
										"rhs_weap_m21a_pr_pbg40",
										"rhs_weap_m21a_fold",
										"rhs_weap_m21a_pbg40",
										"rhs_weap_m21s",
										"rhs_weap_m21s_pr",
										"rhs_weap_m21s_fold",
										"rhs_weap_m82a1",
										"rhs_weap_minimi_para_railed",
										"rhs_weap_g36c",
										"rhs_weap_g36kv",
										"rhs_weap_g36kv_ag36",
										"rhs_weap_m84",
										"rhs_weap_m249_pip_L",
										"rhs_weap_m249_pip_L_para",
										"rhs_weap_m249_pip_L_vfg",
										"rhs_weap_m249_pip_S",
										"rhs_weap_m249_pip_S_para",
										"rhs_weap_m249_pip_S_vfg",
										"rhs_weap_m240B",
										"rhs_weap_m240B_CAP",
										"rhs_weap_m240G",
										"rhsusf_weap_MP7A2",
										"rhsusf_weap_MP7A2_desert",
										"rhsusf_weap_MP7A2_aor1",
										"rhsusf_weap_MP7A2_winter",
										"rhs_weap_hk416d10",
										"rhs_weap_hk416d10_LMT",
										"rhs_weap_hk416d10_m320",
										"rhs_weap_hk416d145",
										"rhs_weap_hk416d145_m320",
										"rhs_weap_m16a4",
										"rhs_weap_m16a4_carryhandle",
										"rhs_weap_m16a4_carryhandle_M203",
										"rhs_weap_m16a4_carryhandle_pmag",
										"rhs_weap_m4",
										"rhs_weap_m4_carryhandle",
										"rhs_weap_m4_carryhandle_pmag",
										"rhs_weap_m4_m203",
										"rhs_weap_m4_m320",
										"rhs_weap_m4a1",
										"rhs_weap_m4a1_blockII",
										"rhs_weap_m4a1_blockII_KAC",
										"rhs_weap_m4a1_blockII_KAC_bk",
										"rhs_weap_m4a1_blockII_KAC_d",
										"rhs_weap_m4a1_blockII_KAC_wd",
										"rhs_weap_m4a1_blockII_M203",
										"rhs_weap_m4a1_blockII_M203_bk",
										"rhs_weap_m4a1_blockII_M203_d",
										"rhs_weap_m4a1_blockII_M203_wd",
										"rhs_weap_m4a1_blockII_bk",
										"rhs_weap_m4a1_blockII_d",
										"rhs_weap_m4a1_blockII_wd",
										"rhs_weap_m4a1_carryhandle",
										"rhs_weap_m4a1_carryhandle_m203",
										"rhs_weap_m4a1_carryhandle_pmag",
										"rhs_weap_m4a1_m203",
										"rhs_weap_m4a1_m320",
										"rhs_weap_mk18",
										"rhs_weap_mk18",
										"rhs_weap_mk18_KAC",
										"rhs_weap_mk18_KAC_bk",
										"rhs_weap_mk18_KAC_d",
										"rhs_weap_mk18_KAC_wd",
										"rhs_weap_mk18_bk",
										"rhs_weap_mk18_d",
										"rhs_weap_mk18_m320",
										"rhs_weap_mk18_wd"
										
									];
ZCP_DMS_BoxFood =						[							// List of food that can spawn in a crate.
											"Exile_Item_GloriousKnakworst_Cooked",
											"Exile_Item_Surstromming_Cooked",
											"Exile_Item_SausageGravy_Cooked",
											"Exile_Item_ChristmasTinner_Cooked",
											"Exile_Item_BBQSandwich_Cooked",
											"Exile_Item_Catfood_Cooked",
											"Exile_Item_DogFood_Cooked",
											"Exile_Item_EMRE",
											"Exile_Item_BeefParts",
											"Exile_Item_Noodles",
											"Exile_Item_SeedAstics",
											"Exile_Item_Raisins",
											"Exile_Item_Moobar",
											"Exile_Item_InstantCoffee"
									];
ZCP_DMS_BoxDrinks =						[
											"Exile_Item_PlasticBottleCoffee",
											"Exile_Item_PowerDrink",
											"Exile_Item_PlasticBottleFreshWater",
											"Exile_Item_EnergyDrink",
											"Exile_Item_MountainDupe",
											"Exile_Item_ChocolateMilk"
									];
ZCP_DMS_BoxMeds =						[
										"Exile_Item_InstaDoc",
										"Exile_Item_Vishpirin",
										"Exile_Item_Bandage"
									];
ZCP_DMS_BoxSurvivalSupplies	=			[							//List of survival supplies (food/drink/meds) that can spawn in a crate. "ZCP_DMS_BoxFood", "ZCP_DMS_BoxDrinks", and "ZCP_DMS_BoxMeds" is automatically added to this list.
										"Exile_Item_Matches",
										"Exile_Item_CookingPot",
										"Exile_Melee_Axe",
										"Exile_Item_CanOpener"
									] + ZCP_DMS_BoxFood + ZCP_DMS_BoxDrinks + ZCP_DMS_BoxMeds;
ZCP_DMS_BoxBaseParts =					[
										"Exile_Item_CamoTentKit",
										"Exile_Item_WoodWallKit",
										"Exile_Item_WoodWallHalfKit",
										"Exile_Item_WoodDoorwayKit",
										"Exile_Item_WoodDoorKit",
										"Exile_Item_WoodFloorKit",
										"Exile_Item_WoodFloorPortKit",
										"Exile_Item_WoodStairsKit",
										"Exile_Item_WoodSupportKit",
										"Exile_Item_FortificationUpgrade",
										"Exile_Item_ConcreteWallKit",
										"Exile_Item_ConcreteWindowKit",
										"Exile_Item_ConcreteDoorKit",
										"Exile_Item_ConcreteDoorwayKit",
										"Exile_Item_ConcreteGateKit",
										"Exile_Item_ConcreteFloorKit",
										"Exile_Item_ConcreteFloorPortKit",
										"Exile_Item_ConcreteStairsKit",
										"EBM_Brickwall_window_Kit",
										"EBM_Brickwall_stairs_Kit",
										"EBM_Brickwall_floorport_door_Kit",
										"EBM_Brickwall_floorport_Kit",
										"EBM_Brickwall_floor_Kit",
										"EBM_Brickwall_door_Kit",
										"EBM_Brickwall_hole_Kit",
										"EBM_Helipad_Kit",
										"EBM_Airhook_Kit",
										"EBM_Parksign_Kit",
										"EBM_Brickwall_Kit"
									];
ZCP_DMS_BoxCraftingMaterials =			[
										"Exile_Item_MetalPole",
										"Exile_Item_MetalBoard",
										"Exile_Item_JunkMetal"
									];
ZCP_DMS_BoxTools =						[
										"Exile_Item_Grinder",
										"Exile_Item_Handsaw"
									];
ZCP_DMS_BoxBuildingSupplies	=			[							// List of building supplies that can spawn in a crate ("ZCP_DMS_BoxBaseParts", "ZCP_DMS_BoxCraftingMaterials", and "ZCP_DMS_BoxTools" are automatically added to this list. "ZCP_DMS_BoxCraftingMaterials" is added twice for weight.)
										"Exile_Item_DuctTape",
										"Exile_Item_PortableGeneratorKit"
									] + ZCP_DMS_BoxBaseParts + ZCP_DMS_BoxCraftingMaterials + ZCP_DMS_BoxCraftingMaterials + ZCP_DMS_BoxTools;
ZCP_DMS_BoxOptics =						[							// List of optics that can spawn in a crate
										"optic_Arco",
										"optic_Hamr",
										"optic_Aco",
										"optic_Holosight",
										"optic_MRCO",
										"optic_SOS",
										"optic_DMS",
										"optic_LRPS"
										//"optic_Nightstalker"			// Nightstalker scope lost thermal in Exile v0.9.4
									];
ZCP_DMS_BoxBackpacks =					[							//List of backpacks that can spawn in a crate
										"B_Bergen_rgr",
										"B_Carryall_oli",
										"B_Kitbag_mcamo",
										"B_Carryall_cbr",
										"B_FieldPack_oucamo",
										"B_FieldPack_cbr",
										"B_Bergen_blk"
									];
ZCP_DMS_BoxItems						= ZCP_DMS_BoxSurvivalSupplies+ZCP_DMS_BoxBuildingSupplies+ZCP_DMS_BoxOptics;	// Random "items" can spawn optics, survival supplies, or building supplies

ZCP_DMS_RareLoot						= true;						// Potential chance to spawn rare loot in any crate.
ZCP_DMS_RareLootList =					[							// List of rare loot to spawn
										"Exile_Item_SafeKit",
										"Exile_Item_CodeLock"
									];
ZCP_DMS_RareLootChance	= 50;						// Percentage Chance to spawn rare loot in any crate | Default: 10%

// Vehicles
ZCP_DMS_ArmedVehicles =					[							// List of armed vehicles that can spawn
											"CUP_BAF_Jackal2_GMG_W",
											"CUP_BAF_Jackal2_L2A1_W",
											"CUP_B_BAF_Coyote_L2A1_W",
											"CUP_B_Dingo_GER_Wdl",
											"CUP_B_FV432_Bulldog_GB_W_RWS",
											"CUP_B_HMMWV_AGS_GPK_ACR",
											"CUP_B_HMMWV_Crows_M2_USA",
											"CUP_B_HMMWV_Crows_MK19_USA",
											"CUP_B_HMMWV_DSHKM_GPK_ACR",
											"CUP_B_HMMWV_M1114_USMC",
											"CUP_B_HMMWV_M2_GPK_USA",
											"CUP_B_HMMWV_M2_USA",
											"CUP_B_HMMWV_M2_USMC",
											"CUP_B_HMMWV_MK19_USMC",
											"CUP_B_HMMWV_SOV_USA",
											"CUP_B_HMMWV_Avenger_USMC",
											"CUP_B_HMMWV_Avenger_USA",
											"CUP_B_HMMWV_Avenger_NATO_T",
											"CUP_B_LAV25M240_USMC",
											"CUP_B_LAV25_HQ_USMC",
											"CUP_B_LAV25_USMC",
											"CUP_B_LR_MG_CZ_W",
											"CUP_B_LR_MG_GB_W",
											"CUP_B_LR_Special_CZ_W",
											"CUP_B_Mastiff_HMG_GB_W",
											"CUP_B_Ridgback_HMG_GB_W",
											"CUP_I_Datsun_PK_Random",
											"CUP_I_SUV_Armored_ION",
											"CUP_I_UAZ_MG_UN",
											"CUP_O_BRDM2_CHDKZ",
											"CUP_O_BRDM2_HQ_CHDKZ",
											"CUP_O_BRDM2_HQ_SLA",
											"CUP_O_BRDM2_SLA",
											"CUP_O_BTR90_HQ_RU",
											"CUP_O_BTR90_RU",
											"CUP_O_Datsun_PK_Random",
											"CUP_O_GAZ_Vodnik_PK_RU",
											"CUP_O_LR_MG_TKA",
											"CUP_O_LR_MG_TKM",
											"CUP_O_UAZ_AGS30_RU",
											"CUP_O_UAZ_MG_RU"
									];

ZCP_DMS_MilitaryVehicles =				[							// List of military vehicles that can spawn
											"Exile_Car_Strider",
											"Exile_Car_Hunter",
											"Exile_Car_Ifrit",
											"CUP_B_HMMWV_Transport_USA",
											"CUP_B_HMMWV_Unarmed_USA",
											"CUP_B_HMMWV_Unarmed_USMC",
											"CUP_C_UAZ_Open_TK_CIV",
											"CUP_I_BTR40_TKG",
											"CUP_O_UAZ_Open_RU",
											"CUP_O_UAZ_Unarmed_RU"
									];

ZCP_DMS_TransportTrucks =				[							// List of transport trucks that can spawn
											"Exile_Car_Van_Guerilla01",
											"Exile_Car_Zamak",
											"Exile_Car_Tempest",
											"Exile_Car_HEMMT",
											"Exile_Car_Ural_Open_Military",
											"Exile_Car_Ural_Covered_Military",
											"CUP_B_LR_Transport_CZ_W",
											"CUP_B_LR_Transport_GB_W",
											"CUP_O_Ural_Empty_RU",
											"CUP_O_Ural_Open_RU",
											"CUP_O_Ural_RU",
											"CUP_O_Ural_Repair_RU"
									];

ZCP_DMS_RefuelTrucks =					[							// List of refuel trucks that can spawn
											"Exile_Car_Van_Fuel_Black",
											"Exile_Car_Van_Fuel_White",
											"Exile_Car_Van_Fuel_Red",
											"Exile_Car_Van_Fuel_Guerilla01",
											"Exile_Car_Van_Fuel_Guerilla02",
											"Exile_Car_Van_Fuel_Guerilla03",
											"CUP_O_Ural_Refuel_RU",
											"RHS_Ural_Flat_MSV_01",
											"RHS_Ural_Flat_VDV_01",
											"RHS_Ural_Flat_VMF_01",
											"RHS_Ural_Flat_VV_01",
											"RHS_Ural_Fuel_MSV_01",
											"RHS_Ural_Fuel_VDV_01",
											"RHS_Ural_Fuel_VMF_01",
											"RHS_Ural_Fuel_VV_01"
									];

ZCP_DMS_CivilianVehicles =				[							// List of civilian vehicles that can spawn
											"Exile_Car_SUV_Red",
											"Exile_Car_Hatchback_Rusty1",
											"Exile_Car_Hatchback_Rusty2",
											"Exile_Car_Hatchback_Sport_Red",
											"Exile_Car_SUV_Red",
											"Exile_Car_Offroad_Rusty2",
											"Exile_Bike_QuadBike_Fia",
											"Fox_ChallengerBR",
											"Fox_ChallengerDev",
											"Fox_ChallengerDev2",
											"Fox_ChallengerYB",
											"Fox_ChallengerO",
											"Fox_ChallengerW",
											"Fox_DaytonaStratos",
											"Fox_CobraR_police",
											"Fox_Outsider",
											"Fox_GNX",
											"Fox_Charger_Apocalypse",
											"Fox_Viper",
											"Fox_F40",
											"Fox_Patrol",
											"Fox_Interceptor",
											"Fox_DaytonaGeneral",
											"Fox_Daytona",
											"Fox_Patrol2",
											"Fox_Patrol3",
											"Fox_BUS",
											"Fox_Megabus",
											"Fox_Fireengine",
											"Fox_LandCruiser",
											"Fox_Tahoe_Apocalypse",
											"Fox_Pickup_Apocalypse",
											"Fox_Tahoe_Apocalypse",
											"Fox_Landrover",
											"Fox_Landrover2",
											"Fox_Pickup",
											"Fox_Pickup_Tow",
											"Fox_Pickup6S",
											"Fox_LandCruiserFox",
											"Fox_LandCruiser3",
											"Fox_LandCruiser2",
											"Fox_Truck",
											"HAP_IF_blue_1",
											"HAP_IF_green_1",
											"HAP_IF_orange_1",
											"HAP_IF_pink_1",
											"HAP_IF_pirate_1",
											"HAP_IF_purple_1",
											"HAP_IF_red_1",
											"HAP_IF_snake_1",
											"HAP_IF_soa_black_1",
											"HAP_IF_winter_1",
											"HAP_IF_woodland_1",
											"HAP_MRAP_HUNTRS_1",
											"HAP_MRAP_blue_1",
											"HAP_MRAP_bomber_1",
											"HAP_MRAP_green_1",
											"HAP_MRAP_hobo_1",
											"HAP_MRAP_orange_1",
											"HAP_MRAP_red_1",
											"HAP_MRAP_snake_1",
											"HAP_MRAP_winter_1",
											"HAP_STRY_HUNTR_1",
											"HAP_STRY_desert1",
											"HAP_STRY_green1",
											"HAP_STRY_urban1",
											"HAP_STRY_woodland1",
											"HAP_ATV_new_black",
											"HAP_ATV_new_blue",
											"HAP_ATV_new_green",
											"HAP_ATV_new_orange",
											"HAP_ATV_new_yellow",
											"HAP_ATV_old_blue",
											"HAP_ATV_old_red",
											"HAP_ATV_old_teal",
											"HAP_OFFRD_HUNTR_1",
											"HAP_OFFRD_HUNTR_2",
											"HAP_OFFRD_autumn",
											"HAP_OFFRD_bandit",
											"HAP_OFFRD_banhammer",
											"HAP_OFFRD_digi_BW",
											"HAP_OFFRD_digi_red",
											"HAP_OFFRD_exilelogo",
											"HAP_OFFRD_generallee",
											"HAP_OFFRD_hemi",
											"HAP_OFFRD_hobo1",
											"HAP_OFFRD_kitty",
											"HAP_OFFRD_larry",
											"HAP_OFFRD_police1",
											"HAP_OFFRD_racer_BW",
											"HAP_OFFRD_racer_RW",
											"HAP_OFFRD_rustbucket1",
											"HAP_OFFRD_rustbucket2",
											"HAP_OFFRD_rustbucket3",
											"HAP_OFFRD_rustbucket_racer1",
											"HAP_OFFRD_woodland",
											"HAP_SUV_blue",
											"HAP_SUV_bmw1",
											"HAP_SUV_cop_black",
											"HAP_SUV_cop_blue",
											"HAP_SUV_medic",
											"HAP_SUV_red",
											"FMP_Hunter_Alpha",
											"FMP_Hunter_Bgcamo",
											"FMP_Hunter_Grunge",
											"FMP_Hunter_Milsim",
											"FMP_Hunter_Ocamo",
											"FMP_Hunter_Savage",
											"FMP_Hunter_Skull",
											"FMP_Hunter_Suba",
											"FMP_Strider_Carbon",
											"FMP_Strider_Hex",
											"FMP_Strider_Multi",
											"FMP_Strider_Radioactive",
											"FMP_Strider_Rusty",
											"FMP_Strider_Splatter",
											"FMP_Strider_Standard",
											"bv_458_aqua",
											"bv_458_baby_blue",
											"bv_458_baby_pink",
											"bv_458_black2",
											"bv_458_burgundy",
											"bv_458_cardinal",
											"bv_458_dark_green",
											"bv_458_gold",
											"bv_458_green",
											"bv_458_grey",
											"bv_458_lavender",
											"bv_458_light_blue",
											"bv_458_light_yellow",
											"bv_458_lime",
											"bv_458_marina_blue",
											"bv_458_navy_blue",
											"bv_458_orange",
											"bv_458_pink",
											"bv_458_purple",
											"bv_458_red",
											"bv_458_sand",
											"bv_458_silver",
											"bv_458_skin_blue",
											"bv_458_skin_camo",
											"bv_458_skin_camo_urban",
											"bv_ben_dover_aqua",
											"bv_ben_dover_baby_blue",
											"bv_ben_dover_baby_pink",
											"bv_ben_dover_black2",
											"bv_ben_dover_burgundy",
											"bv_ben_dover_cardinal",
											"bv_ben_dover_dark_green",
											"bv_ben_dover_gold",
											"bv_ben_dover_green",
											"bv_ben_dover_grey",
											"bv_ben_dover_lavender",
											"bv_ben_dover_light_blue",
											"bv_ben_dover_light_yellow",
											"bv_ben_dover_lime",
											"bv_ben_dover_marina_blue",
											"bv_ben_dover_navy_blue",
											"bv_ben_dover_orange",
											"bv_ben_dover_pink",
											"bv_ben_dover_purple",
											"bv_ben_dover_red",
											"bv_ben_dover_sand",
											"bv_ben_dover_silver",
											"bv_ben_dover_skin_blue",
											"bv_ben_dover_skin_camo",
											"bv_ben_dover_skin_camo_urban",
											"bv_caressa_gt_aqua",
											"bv_caressa_gt_baby_blue",
											"bv_caressa_gt_baby_pink",
											"bv_caressa_gt_black2",
											"bv_caressa_gt_burgundy",
											"bv_caressa_gt_cardinal",
											"bv_caressa_gt_dark_green",
											"bv_caressa_gt_gold",
											"bv_caressa_gt_green",
											"bv_caressa_gt_grey",
											"bv_caressa_gt_lavender",
											"bv_caressa_gt_light_blue",
											"bv_caressa_gt_light_yellow",
											"bv_caressa_gt_lime",
											"bv_caressa_gt_marina_blue",
											"bv_caressa_gt_navy_blue",
											"bv_caressa_gt_orange",
											"bv_caressa_gt_pink",
											"bv_caressa_gt_purple",
											"bv_caressa_gt_red",
											"bv_caressa_gt_sand",
											"bv_caressa_gt_silver",
											"bv_caressa_gt_skin_blue",
											"bv_caressa_gt_skin_camo",
											"bv_caressa_gt_skin_camo_urban",
											"bv_e60_m5_aqua",
											"bv_e60_m5_baby_blue",
											"bv_e60_m5_baby_pink",
											"bv_e60_m5_black2",
											"bv_e60_m5_burgundy",
											"bv_e60_m5_cardinal",
											"bv_e60_m5_dark_green",
											"bv_e60_m5_gold",
											"bv_e60_m5_green",
											"bv_e60_m5_grey",
											"bv_e60_m5_lavender",
											"bv_e60_m5_light_blue",
											"bv_e60_m5_light_yellow",
											"bv_e60_m5_lime",
											"bv_e60_m5_marina_blue",
											"bv_e60_m5_navy_blue",
											"bv_e60_m5_orange",
											"bv_e60_m5_pink",
											"bv_e60_m5_purple",
											"bv_e60_m5_red",
											"bv_e60_m5_sand",
											"bv_e60_m5_silver",
											"bv_e60_m5_skin_blue",
											"bv_e60_m5_skin_camo",
											"bv_e60_m5_skin_camo_urban",
											"bv_e63_amg_aqua",
											"bv_e63_amg_baby_blue",
											"bv_e63_amg_baby_pink",
											"bv_e63_amg_black2",
											"bv_e63_amg_burgundy",
											"bv_e63_amg_cardinal",
											"bv_e63_amg_dark_green",
											"bv_e63_amg_gold",
											"bv_e63_amg_green",
											"bv_e63_amg_grey",
											"bv_e63_amg_lavender",
											"bv_e63_amg_light_blue",
											"bv_e63_amg_light_yellow",
											"bv_e63_amg_lime",
											"bv_e63_amg_marina_blue",
											"bv_e63_amg_navy_blue",
											"bv_e63_amg_orange",
											"bv_e63_amg_pink",
											"bv_e63_amg_purple",
											"bv_e63_amg_red",
											"bv_e63_amg_sand",
											"bv_e63_amg_silver",
											"bv_e63_amg_skin_blue",
											"bv_e63_amg_skin_camo",
											"bv_e63_amg_skin_camo_urban",
											"bv_gtr_spec_V_aqua",
											"bv_gtr_spec_V_baby_blue",
											"bv_gtr_spec_V_baby_pink",
											"bv_gtr_spec_V_black2",
											"bv_gtr_spec_V_burgundy",
											"bv_gtr_spec_V_cardinal",
											"bv_gtr_spec_V_dark_green",
											"bv_gtr_spec_V_gold",
											"bv_gtr_spec_V_green",
											"bv_gtr_spec_V_grey",
											"bv_gtr_spec_V_lavender",
											"bv_gtr_spec_V_light_blue",
											"bv_gtr_spec_V_light_yellow",
											"bv_gtr_spec_V_lime",
											"bv_gtr_spec_V_marina_blue",
											"bv_gtr_spec_V_navy_blue",
											"bv_gtr_spec_V_orange",
											"bv_gtr_spec_V_pink",
											"bv_gtr_spec_V_purple",
											"bv_gtr_spec_V_red",
											"bv_gtr_spec_V_sand",
											"bv_gtr_spec_V_silver",
											"bv_gtr_spec_V_skin_blue",
											"bv_gtr_spec_V_skin_camo",
											"bv_gtr_spec_V_skin_camo_urban",
											"bv_shelly_bf_skin",
											"bv_shelly_bw_skin",
											"bv_shelly_rw_skin",
											"bv_shelly_wb_skin",
											"bv_shelly_wbl_skin",
											"bv_shelly_wf_skin",
											"bv_the_crowner_aqua",
											"bv_the_crowner_baby_blue",
											"bv_the_crowner_baby_pink",
											"bv_the_crowner_black2",
											"bv_the_crowner_burgundy",
											"bv_the_crowner_cardinal",
											"bv_the_crowner_dark_green",
											"bv_the_crowner_gold",
											"bv_the_crowner_green",
											"bv_the_crowner_grey",
											"bv_the_crowner_lavender",
											"bv_the_crowner_light_blue",
											"bv_the_crowner_light_yellow",
											"bv_the_crowner_lime",
											"bv_the_crowner_marina_blue",
											"bv_the_crowner_navy_blue",
											"bv_the_crowner_orange",
											"bv_the_crowner_pink",
											"bv_the_crowner_purple",
											"bv_the_crowner_red",
											"bv_the_crowner_sand",
											"bv_the_crowner_silver",
											"bv_the_crowner_skin_blue",
											"bv_the_crowner_skin_camo",
											"bv_the_crowner_skin_camo_urban",
											"bv_the_crowner_skin_white",
											"bv_monster_aqua",
											"bv_monster_baby_blue",
											"bv_monster_baby_pink",
											"bv_monster_black2",
											"bv_monster_burgundy",
											"bv_monster_cardinal",
											"bv_monster_dark_green",
											"bv_monster_gold",
											"bv_monster_green",
											"bv_monster_grey",
											"bv_monster_lavender",
											"bv_monster_light_blue",
											"bv_monster_light_yellow",
											"bv_monster_lime",
											"bv_monster_marina_blue",
											"bv_monster_navy_blue",
											"bv_monster_orange",
											"bv_monster_pink",
											"bv_monster_purple",
											"bv_monster_red",
											"bv_monster_sand",
											"bv_monster_silver",
											"bv_monster_skin_blue",
											"bv_monster_skin_camo",
											"bv_monster_skin_camo_urban",
											"CUP_B_HMMWV_Transport_USA",
											"CUP_B_HMMWV_Unarmed_USA",
											"CUP_B_HMMWV_Unarmed_USMC",
											"CUP_B_LR_Transport_CZ_W",
											"CUP_B_LR_Transport_GB_W",
											"CUP_C_UAZ_Open_TK_CIV",
											"CUP_I_BTR40_TKG",
											"CUP_O_UAZ_Open_RU",
											"CUP_O_UAZ_Unarmed_RU",
											"CUP_O_Ural_Empty_RU",
											"CUP_O_Ural_Open_RU",
											"CUP_O_Ural_RU",
											"CUP_O_Ural_Refuel_RU",
											"CUP_O_Ural_Repair_RU",
											"CUP_BAF_Jackal2_GMG_W",
											"CUP_BAF_Jackal2_L2A1_W",
											"CUP_B_BAF_Coyote_L2A1_W",
											"CUP_B_Dingo_GER_Wdl",
											"CUP_B_FV432_Bulldog_GB_W_RWS",
											"CUP_B_HMMWV_AGS_GPK_ACR",
											"CUP_B_HMMWV_Crows_M2_USA",
											"CUP_B_HMMWV_Crows_MK19_USA",
											"CUP_B_HMMWV_DSHKM_GPK_ACR",
											"CUP_B_HMMWV_M1114_USMC",
											"CUP_B_HMMWV_M2_GPK_USA",
											"CUP_B_HMMWV_M2_USA",
											"CUP_B_HMMWV_M2_USMC",
											"CUP_B_HMMWV_MK19_USMC",
											"CUP_B_HMMWV_SOV_USA",
											"CUP_B_HMMWV_Avenger_USMC",
											"CUP_B_HMMWV_Avenger_USA",
											"CUP_B_HMMWV_Avenger_NATO_T",
											"CUP_B_LAV25M240_USMC",
											"CUP_B_LAV25_HQ_USMC",
											"CUP_B_LAV25_USMC",
											"CUP_B_LR_MG_CZ_W",
											"CUP_B_LR_MG_GB_W",
											"CUP_B_LR_Special_CZ_W",
											"CUP_B_Mastiff_HMG_GB_W",
											"CUP_B_Ridgback_HMG_GB_W",
											"CUP_I_Datsun_PK_Random",
											"CUP_I_SUV_Armored_ION",
											"CUP_I_UAZ_MG_UN",
											"CUP_O_BRDM2_CHDKZ",
											"CUP_O_BRDM2_HQ_CHDKZ",
											"CUP_O_BRDM2_HQ_SLA",
											"CUP_O_BRDM2_SLA",
											"CUP_O_BTR90_HQ_RU",
											"CUP_O_BTR90_RU",
											"CUP_O_Datsun_PK_Random",
											"CUP_O_GAZ_Vodnik_PK_RU",
											"CUP_O_LR_MG_TKA",
											"CUP_O_LR_MG_TKM",
											"CUP_O_UAZ_AGS30_RU",
											"CUP_O_UAZ_MG_RU",
											"CUP_B_AH1Z_NoWeapons"
									];

ZCP_DMS_TransportHelis =				[							// List of transport helis that can spawn
										"Exile_Chopper_Hummingbird_Green",
										"Exile_Chopper_Orca_BlackCustom",
										"Exile_Chopper_Mohawk_FIA",
										"Exile_Chopper_Huron_Black",
										"Exile_Chopper_Hellcat_Green",
										"Exile_Chopper_Taru_Transport_Black",
										"CUP_I_UH60L_Unarmed_FFV_Racs",
										"CUP_I_UH60L_Unarmed_RACS"
									];

ZCP_VehicleReward = ZCP_DMS_ArmedVehicles;
ZCP_DMS_DEBUG = false;




ZCP_CurrentMod = "Exile"; // Exile, ( Epoch coming soon again)

/* Do not change this*/
ZCP_CapPoints = call ZCP_fnc_missions;
ZCP_Translations = call ZCP_fnc_translations;
diag_log text format["[ZCP]: Config loaded succesfull"];
ZCP_ConfigLoaded = true;
