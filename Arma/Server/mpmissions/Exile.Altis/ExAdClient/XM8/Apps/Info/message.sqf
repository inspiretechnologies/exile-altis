<t align='center' color='#E48A36'>GENERAL RULES AND SERVER INFO</t><br />
<t color='#FFFF00'>There is a lot of information here, please read it all.</t><br />

<t align='center' color='#FFFF00'>Admin :: steve, Thresh, Veronica</t><br />
<t align='center' color='#FFFFFF'>No PvP outside of the marked PvP zones, check the map.</t><br />
<t align='center' color='#FFFFFF'>Both players must be fully inside the PvP zone.</t><br />
<t align='center' color='#FFFFFF'>Call your missions out in chat before you start them. Mark them on the map with clear markers, in side chat.</t><br />
<t align='center' color='#FFFFFF'>The PvP capture points are always PvP.</t><br />
<t align='center' color='#FFFFFF'>No stealing in safezone with players present.</t><br />
<t align='center' color='#FFFF00'>Other player bases are off limits. NO EXCEPTIONS!.</t><br />
<t align='center' color='#FFFFFF'>No glitching,hacking, duping or exploit of any kind</t><br />
<t align='center' color='#FFFF00'>Anyone caught base raiding will be banned!</t><br />
<t align='center' color='#FFFFFF'>Respect admins. Admins decide. Do not argue with admins!</t><br />
<t align='center' color='#FFFF00'>Rules may change without notice, check back often</t><br />

<t color='#E48A36'>HELP US STAY ONLINE</t><br />
<br /><t color='#FFFFFF'>Consider donating, any amount helps keep us online and keep the server upgrades coming as we grow. Visit http://www.exilefiles.com/</t><br />

______________________________________________________________________________<br />

<t color='#E48A36'>TIPS</t><br />
<br /><t color='#FFFFFF'>Double Click the Radio Slot in your Inventory to deploy your personal vehicle.</t><br />
<br /><t color='#FFFFFF'>You may claim vehicles which have no owner by installing a code lock via the "Claim Ownership" scroll menu option.</t><br />
<br /><t color='#FFFFFF'>No, we don't have Virtual Garage. Too many players experience problems with it. Feel free to park in an orderly fashion in the safe zones.</t><br />

______________________________________________________________________________<br />

<t color='#E48A36'>FEATURES AND MODS</t><br />
<br /><t color='#FFFFFF'>exAd, DMS, ZCP, CUP, RHS, EBM, FMP, HAP, FOX, R3F Crate Loading and Towing, Occupation (Roaming AI), VCom enhanced AI, Bigfoot's Shipwrecks, Advanced Towing, Advanced Sling Loading, Advanced Rappelling, Enigma Revive, Enigma Personal Vehicle. </t><br />
<br /><t color='#FFFF00'>More to come. We are open to suggestions and critique. Please report all problems as it will help us track down bugs and/or misconfigurations. </t><br />
<br /><t color='#FFFF00'>Email: steve@focusinfinite.com</t><br />
______________________________________________________________________________<br />

<br /><t color='#E48A36'>The AI</t><br />
<br /><t color='#FFFFFF'> There are roaming AI on.</t><br />
<br /><t color='#FFFF00'> AI in orange jumpsuits are on your side unless you shoot them.</t><br />
<br /><t color='#FFFFFF'> AI on server are smarter, they will steal unlocked vehicles.</t><br />
<br /><t color='#FFFF00'> AI can have armed vehicles, helicopters, boats and RPG launchers.</t><br />
______________________________________________________________________________<br />


<t color='#E48A36'>BUILDING RULES</t><br />
<t color='#FFFFFF'>Must be approximately 1000 meter from safezones.</t><br />
<t color='#FFFF00'>No building on or over roads.</t><br />
<t color='#FFFFFF'>No blocking of loot areas with base building</t><br />
<t color='#FFFFFF'>No building on Military Bases, Towns, or Airfields.</t><br />
<t color='#FFFF00'>Build your base to repel an AI attack! </t><br />
<t color='#FFFFFF'>No building on a Re-Arm point.</t><br />
<t color='#FFFF00'>Flag can not be placed in an way that makes it unaccessible. aka below ground floor, or floating in the air.</t><br />
<t color='#FFFFFF'>Bases Breaking these rules will get their flag removed!</t><br />
<t color='#FFFF00'>If its an awesome loot spot, with military buildings, and you are surprised no one has put a flag down there - it is probably because its against the rules. ASK IF UNSURE! We can't move bases!</t><br />

______________________________________________________________________________<br />



