	///////////////////////////////////////////////////////////////////////////////
	// FOX Vehicles Vehicles
	// Thanks to Bob_the_K for the list
	///////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////
	// FOX Cars
	///////////////////////////////////////////////////////////////////////////////

	class Fox_ChallengerBR			{ quality = 1; price = 11000; };
	class Fox_ChallengerDev			{ quality = 1; price = 11000; };
	class Fox_ChallengerDev2		{ quality = 1; price = 11000; };
	class Fox_ChallengerYB			{ quality = 1; price = 11000; };
	class Fox_ChallengerO			{ quality = 1; price = 11000; };
	class Fox_ChallengerW			{ quality = 1; price = 11000; };
	class Fox_DaytonaStratos		{ quality = 1; price = 15000; };
	class Fox_DaytonaGeneral		{ quality = 1; price = 15000; };
	class Fox_Daytona				{ quality = 1; price = 15000; };
	class Fox_CobraR_police			{ quality = 1; price = 18000; };
	class Fox_Outsider				{ quality = 1; price = 19000; };
	class Fox_GNX					{ quality = 1; price = 22000; };
	class Fox_Charger_Apocalypse	{ quality = 1; price = 24000; };
	class Fox_Viper					{ quality = 1; price = 25000; };
	class Fox_F40					{ quality = 1; price = 30000; };
	class Fox_Patrol				{ quality = 1; price = 22000; };
	class Fox_Patrol2				{ quality = 1; price = 22000; };
	class Fox_Patrol3				{ quality = 1; price = 22000; };
	class Fox_Interceptor			{ quality = 1; price = 29000; };

	///////////////////////////////////////////////////////////////////////////////
	// FOX Trucks
	///////////////////////////////////////////////////////////////////////////////

	class Fox_BUS					{ quality = 1; price = 31000; };
	class Fox_Megabus				{ quality = 1; price = 100000; };
	class Fox_Fireengine			{ quality = 1; price = 90000; };
	class Fox_LandCruiser			{ quality = 1; price = 30000; };
	class Fox_LandCruiserFox		{ quality = 1; price = 30000; };
	class Fox_LandCruiser3			{ quality = 1; price = 30000; };
	class Fox_LandCruiser2			{ quality = 1; price = 30000; };
	class Fox_Tahoe_Apocalypse		{ quality = 1; price = 35000; };
	class Fox_Truck					{ quality = 1; price = 40000; };
	class Fox_Pickup_Apocalypse		{ quality = 1; price = 50000; };
	class Fox_Pickup				{ quality = 1; price = 51000; };
	class Fox_Pickup_Tow			{ quality = 1; price = 55000; };
	class Fox_Pickup6S				{ quality = 1; price = 57000; };
	class Fox_Landrover				{ quality = 1; price = 61000; };
	class Fox_Landrover2			{ quality = 1; price = 61000; };