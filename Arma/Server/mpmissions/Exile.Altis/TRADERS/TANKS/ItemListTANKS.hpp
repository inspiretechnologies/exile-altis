	///////////////////////////////////////////////////////////////////////////////
	// Tanks DLC
	///////////////////////////////////////////////////////////////////////////////

	class B_AFV_Wheeled_01_cannon_F                        { quality = 3; price = 500000; sellPrice = 150000; };      // Rhino MGS
    class B_T_AFV_Wheeled_01_cannon_F                      { quality = 3; price = 500000; sellPrice = 150000; };       // Rhino MGS
    class B_AFV_Wheeled_01_up_cannon_F                     { quality = 3; price = 650000; sellPrice = 175000; };       // Rhino MGS UP
    class B_T_AFV_Wheeled_01_up_cannon_F                   { quality = 3; price = 650000; sellPrice = 175000; };       // Rhino MGS UP
    
    class I_LT_01_AT_F                                     { quality = 2; price = 250000; sellPrice = 100000; };      // AWC 301 Nyx (AT)        
    class I_LT_01_scout_F                                  { quality = 2; price = 175000; sellPrice = 75000; };       // AWC 303 Nyx (Recon)     
    class I_LT_01_AA_F                                     { quality = 2; price = 250000; sellPrice = 100000; };       // AWC 302 Nyx (AA)        
    class I_LT_01_cannon_F                                 { quality = 2; price = 250000; sellPrice = 100000; };       // AWC 304 Nyx (Autocannon)
        
    class O_MBT_04_cannon_F                                { quality = 4; price = 1000000; sellPrice = 250000; };      // T-140 Angara 
    class O_T_MBT_04_cannon_F                              { quality = 4; price = 1000000; sellPrice = 250000; };       // T-140 Angara 
    class O_MBT_04_command_F                               { quality = 4; price = 1000000; sellPrice = 250000; };       // T-140K Angara
    class O_T_MBT_04_command_F                             { quality = 4; price = 1000000; sellPrice = 250000; };       // T-140K Angara
	
	
	 ///////////////////////////////////////////////////////////////////////////////
    // Tanks DLC Rocket Launchers
    ///////////////////////////////////////////////////////////////////////////////    
    
    class launch_O_Vorona_brown_F                   { quality = 1; price = 10000; sellPrice = 4000; };     // 9M135 Vorona (Brown)
    class launch_O_Vorona_green_F                   { quality = 1; price = 10000; sellPrice = 4000; };     // 9M135 Vorona (Green)
    class launch_MRAWS_olive_F                      { quality = 1; price = 10000; sellPrice = 4000; };     // MAAWS Mk4 Mod 1 (Olive)
    class launch_MRAWS_olive_rail_F                 { quality = 1; price = 10000; sellPrice = 4000; };     // MAAWS Mk4 Mod 0 (Olive)
    class launch_MRAWS_green_F                      { quality = 1; price = 10000; sellPrice = 4000; };     // MAAWS Mk4 Mod 1 (Green)
    class launch_MRAWS_green_rail_F                 { quality = 1; price = 10000; sellPrice = 4000; };     // MAAWS Mk4 Mod 0 (Green)
    class launch_MRAWS_sand_F                       { quality = 1; price = 10000; sellPrice = 4000; };     // MAAWS Mk4 Mod 1 (Sand) 
    class launch_MRAWS_sand_rail_F                  { quality = 1; price = 10000; sellPrice = 4000; };     // MAAWS Mk4 Mod 0 (Sand)    

    ///////////////////////////////////////////////////////////////////////////////
    // Tanks DLC Rockets
    ///////////////////////////////////////////////////////////////////////////////    
    
    class Vorona_HEAT                               { quality = 1; price = 5000; sellPrice = 2000; };      // 9M135 HEAT Missile
    class Vorona_HE                                 { quality = 1; price = 5000; sellPrice = 2000; };      // 9M135 HE Missile  
    class MRAWS_HEAT_F                              { quality = 1; price = 5000; sellPrice = 2000; };      // MAAWS HEAT 75 Round
    class MRAWS_HE_F                                { quality = 1; price = 5000; sellPrice = 2000; };      // MAAWS HE 44 Round 