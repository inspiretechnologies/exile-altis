class DLCTanks
    {
        name = "DLC Tanks";
        icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
        items[]=
        {            
            "B_AFV_Wheeled_01_cannon_F",                           // Rhino MGS
            "B_T_AFV_Wheeled_01_cannon_F",                         // Rhino MGS
            "B_AFV_Wheeled_01_up_cannon_F",                        // Rhino MGS UP
            "B_T_AFV_Wheeled_01_up_cannon_F",                      // Rhino MGS UP
            "I_LT_01_AT_F",                                        // AWC 301 Nyx (AT)        
            "I_LT_01_scout_F",                                     // AWC 303 Nyx (Recon)     
            "I_LT_01_AA_F",                                        // AWC 302 Nyx (AA)        
            "I_LT_01_cannon_F",                                    // AWC 304 Nyx (Autocannon)
            "O_MBT_04_cannon_F",                                   // T-140 Angara 
            "O_T_MBT_04_cannon_F",                                 // T-140 Angara 
            "O_MBT_04_command_F",                                  // T-140K Angara
            "O_T_MBT_04_command_F"                                 // T-140K Angara
        };
    };
	
 class DLCRocketLaunchers
	{
		name = "DLC Rocket Launchers";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] = 
		{
			"launch_O_Vorona_brown_F",        // 9M135 Vorona (Brown)
			"launch_O_Vorona_green_F",        // 9M135 Vorona (Green) 
			"launch_MRAWS_olive_F",           // MAAWS Mk4 Mod 1 (Olive)    
			"launch_MRAWS_olive_rail_F",      // MAAWS Mk4 Mod 0 (Olive)    
			"launch_MRAWS_green_F",           // MAAWS Mk4 Mod 1 (Green)    
			"launch_MRAWS_green_rail_F",      // MAAWS Mk4 Mod 0 (Green)    
			"launch_MRAWS_sand_F",            // MAAWS Mk4 Mod 1 (Sand)     
			"launch_MRAWS_sand_rail_F"        // MAAWS Mk4 Mod 0 (Sand)     
		};
	};

class DLCRocketLauncherAmmo
	{
		name = "DLC Rocket Launcher Ammo";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] = 
		{    
			"Vorona_HEAT",                    // 9M135 HEAT Missile
			"Vorona_HE",                      // 9M135 HE Missile              
			"MRAWS_HEAT_F",                   // MAAWS HEAT 75 Round            
			"MRAWS_HE_F"                      // MAAWS HE 44 Round              
		};
	};    