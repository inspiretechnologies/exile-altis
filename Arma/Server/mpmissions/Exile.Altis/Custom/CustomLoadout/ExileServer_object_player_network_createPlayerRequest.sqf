/**
 * ExileServer_object_player_network_createPlayerRequest
 *
 * Exile Mod
 * www.exilemod.com
 * © 2015 Exile Mod Team
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */
 
private["_sessionID", "_parameters", "_requestingPlayer", "_spawnLocationMarkerName", "_playerUID", "_accountData", "_bambiPlayer", "_cargoType"];
_sessionID = _this select 0;
_parameters = _this select 1;
_requestingPlayer = _sessionID call ExileServer_system_session_getPlayerObject;
try
{
	if (isNull _requestingPlayer) then 
	{
		throw format ["Session %1 requested a bambi character, but doesn't have a player object. Hacker or Monday?", _sessionID];
	};
	_spawnLocationMarkerName = _parameters select 0;
	_playerUID = getPlayerUID _requestingPlayer;
	if(_playerUID isEqualTo "")then
	{
		throw format ["Player: '%1' has no player UID. Arma/Steam sucks!.",name _requestingPlayer];
	};
	_accountData = format["getAccountStats:%1", _playerUID] call ExileServer_system_database_query_selectSingle;
	_bambiPlayer = (createGroup independent) createUnit ["Exile_Unit_Player", [0,0,0], [], 0, "CAN_COLLIDE"];
	removeHeadgear _bambiPlayer;
	
	
	// Custom Loadout Starts Here. Uncomment the lines you want your players to have..
    
    // Clothing
    _bambiPlayer forceAddUniform "HAP_POLO_Hello"; // Hello Kitty Polo and White Jorts
    //_bambiPlayer addVest "V_PlateCarrierGL_rgr"; // Change Vest Class Here
    //_bambiPlayer addHeadgear "H_Cap_oli"; // Change Headgear Class Here
    //_bambiPlayer addBackpack "B_Carryall_oli"; // Change Backpack Class Here
    
    // Navigation Items
    _bambiPlayer linkItem "ItemGPS"; // This Puts The GPS Into The Correct Slot
    _bambiPlayer linkItem "Exile_Item_XM8"; // This Puts The XM8 Into The Correct Slot 
    _bambiPlayer linkItem "ItemCompass"; //This Puts The Compass Into The Correct Slot
    _bambiPlayer linkItem "ItemMap"; //This Puts The Map Into The Correct Slot
    _bambiPlayer linkItem "ItemRadio"; //This Puts The Radio Into The Correct Slot

    // Food and Drink Items
    _bambiPlayer addItem "Exile_Item_PlasticBottleCoffee"; // Change Drink Class Here
    _bambiPlayer addItem "Exile_Item_EMRE"; // Change Food Class Here
    
    // Medical Items
   // _bambiPlayer addItem "Exile_Item_InstaDoc"; // Change Meds Class Here

    // Items
    //_bambiPlayer addItem "Exile_Item_DuctTape";
    
    // Ammo
    //_bambiPlayer addItemToVest "11Rnd_45ACP_Mag"; // Ammo For Weapon Listed Below
    //_bambiPlayer addItemToVest "11Rnd_45ACP_Mag"; // Ammo For Weapon Listed Below    
    //_bambiPlayer addItemToVest "10Rnd_762x54_Mag"; // Ammo For Weapon Listed Below
    //_bambiPlayer addItemToVest "10Rnd_762x54_Mag"; // Ammo For Weapon Listed Below
    //_bambiPlayer addItemToVest "10Rnd_762x54_Mag"; // Ammo For Weapon Listed Below    
    //_bambiPlayer addItemToVest "10Rnd_762x54_Mag"; // Ammo For Weapon Listed Below    
    
    // Weapons
    //_bambiPlayer addWeapon "hgun_Pistol_heavy_01_F"; // Handgun Weapon
    //_bambiPlayer addWeapon "srifle_DMR_01_F"; // Primary Weapon    
    
    // Weapons Attachments
    //_bambiPlayer addHandgunItem "optic_MRD"; // Adds Attachment to Handgun | Change Attachment Class Here
    //_bambiPlayer addHandgunItem "muzzle_snds_acp"; // Adds Attachment to Handgun | Change Attachment Class Here
    //_bambiPlayer addPrimaryWeaponItem "optic_AMS_khk"; // Adds Attachment to Primary Weapon | Change Attachment Class Here  
    //_bambiPlayer addPrimaryWeaponItem "muzzle_snds_B_snd_F"; // Adds Attachment to Primary Weapon | Change Attachment Class Here    
    //_bambiPlayer addItemToVest "optic_Nightstalker"; // Adds Attachment to Primary Weapon | Change Attachment Class Here    
    
	// Custom Loadout Ends Here. Uncomment the lines you want your players to have..
	
	
	{
		_cargoType = _x call ExileClient_util_cargo_getType;
		switch (_cargoType) do
		{
			case 1: 	{ _bambiPlayer addItem _x; };
			case 2: 	{ _bambiPlayer addWeaponGlobal _x; };
			case 3: 	{ _bambiPlayer addBackpackGlobal _x; };
			case 4:		{ _bambiPlayer linkItem _x; };
			default 					{ _bambiPlayer addItem _x; };
		};
	}
	forEach getArray(configFile >> "CfgSettings" >> "BambiSettings" >> "loadOut");
	[_sessionID, _requestingPlayer, _spawnLocationMarkerName, _bambiPlayer, _accountData] call ExileServer_object_player_createBambi;
}
catch
{
	_exception call ExileServer_util_log;
};