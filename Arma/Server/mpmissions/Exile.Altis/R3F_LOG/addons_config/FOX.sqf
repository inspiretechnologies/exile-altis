/**
 * Logistics configuration for All in Arma.
 * The configuration is splitted in categories dispatched in the included files.
 */

// Load the logistics config only if the addon is used

#include "FOX\Air.sqf"
#include "FOX\LandVehicle.sqf"
#include "FOX\Ship.sqf"
#include "FOX\Building.sqf"
#include "FOX\ReammoBox.sqf"
#include "FOX\Others.sqf"
