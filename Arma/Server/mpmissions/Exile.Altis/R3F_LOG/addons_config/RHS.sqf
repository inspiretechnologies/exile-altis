/**
 * Logistics configuration for All in Arma.
 * The configuration is splitted in categories dispatched in the included files.
 */

// Load the logistics config only if the addon is used

#include "RHS\Air.sqf"
#include "RHS\LandVehicle.sqf"
#include "RHS\Ship.sqf"
#include "RHS\Building.sqf"
#include "RHS\ReammoBox.sqf"
#include "RHS\Others.sqf"
