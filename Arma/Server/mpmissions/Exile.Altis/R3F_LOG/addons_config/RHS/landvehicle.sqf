R3F_LOG_CFG_can_tow = R3F_LOG_CFG_can_tow +
[
            //Unarmed
    "RHS_Ural_Open_MSV_01",
	"rhsusf_M1078A1P2_WD_open_fmtv_usarmy"
            
            //Armed
			
            
];

R3F_LOG_CFG_can_be_towed = R3F_LOG_CFG_can_be_towed +
[
            //Unarmed
           
            
            //Armed
			
];

R3F_LOG_CFG_can_lift = R3F_LOG_CFG_can_lift +
[
    
];

R3F_LOG_CFG_can_be_lifted = R3F_LOG_CFG_can_be_lifted +
[
            //Unarmed
           
            
            //Armed
			
];

R3F_LOG_CFG_can_transport_cargo = R3F_LOG_CFG_can_transport_cargo +
[
 
		["rhs_tigr_ffv_3camo_vdv",                                     25],
		["rhs_tigr_ffv_vdv",                                           25],
		["rhs_tigr_3camo_vdv",                                         25],
		["rhs_tigr_sts_3camo_vdv",                                     25],
		["rhs_tigr_vdv",                                               25],
		["rhs_gaz66_r142_vmf",                                         50],
		["rhs_gaz66_repair_vmf",                                       50],
		["rhs_gaz66_vmf",                                              50],
		["rhs_gaz66_flat_vdv",                                         50],
		["rhs_gaz66o_vdv",                                             50],
		["rhs_gaz66o_flat_vdv",                                        50],
		["rhs_gaz66_ammo_vmf",                                         50],
		["rhs_gaz66_vv",                                               50],
		["rhs_gaz66_msv",                                              50],
		["rhs_gaz66_flat_vmf",                                         50],
		["rhs_gaz66_flat_vv",                                          50],
		["rhs_gaz66_flat_msv",                                         50],
		["rhs_gaz66o_vmf",                                             50],
		["rhs_gaz66o_vv",                                              50],
		["rhs_gaz66o_msv",                                             50],
		["rhs_gaz66o_flat_vmf",                                        50],
		["rhs_gaz66o_flat_vv",                                         50],
		["rhs_gaz66o_flat_msv",                                        50],
		["rhs_gaz66_r142_vdv",                                         50],
		["rhs_gaz66_r142_msv",                                         50],
		["rhs_gaz66_r142_vv",                                          50],
		["rhs_gaz66_repair_vdv",                                       50],
		["rhs_gaz66_repair_vv",                                        50],
		["rhs_gaz66_repair_msv",                                       50],
		["rhsusf_m113_usarmy_M240",                                    25],
		["rhsusf_m113_usarmy_unarmed",                                 25],
		["rhsusf_m1025_w",                                             25],
		["rhsusf_m1025_w_m2",                                          25],
		["rhsusf_m998_w_2dr",                                          25],
		["rhsusf_m998_w_4dr",                                          25],
		["rhsusf_m998_w_s_2dr_fulltop",                                25],
		["rhsusf_m998_d_s_2dr_fulltop",                                25],
		["rhsusf_m998_w_s_2dr_halftop",                                25],
		["rhsusf_m998_d_s_2dr_halftop",                                25],
		["rhsusf_m998_d_s_4dr_fulltop",                                25],
		["rhsusf_m998_w_s_4dr_fulltop",                                25],
		["rhsusf_m998_d_s_4dr_halftop",                                25],
		["rhsusf_m998_w_s_4dr_halftop",                                25],
		["rhsusf_M1078A1P2_wd_fmtv_usarmy",                            50],
		["rhsusf_M1078A1P2_d_fmtv_usarmy",                             50],
		["rhsusf_M1078A1P2_wd_open_fmtv_usarmy",                       50],
		["rhsusf_M1078A1P2_d_open_fmtv_usarmy",                        50],
		["rhsusf_M1078A1P2_wd_flatbed_fmtv_usarmy",                    50],
		["rhsusf_M1078A1P2_d_flatbed_fmtv_usarmy",                     50],
		["rhsusf_M1078A1P2_B_d_fmtv_usarmy",                           50],
		["rhsusf_M1078A1P2_B_wd_fmtv_usarmy",                          50],
		["rhsusf_M1078A1P2_B_wd_open_fmtv_usarmy",                     50],
		["rhsusf_M1078A1P2_B_d_open_fmtv_usarmy",                      50],
		["rhsusf_M1078A1P2_B_wd_flatbed_fmtv_usarmy",                  50],
		["rhsusf_M1078A1P2_B_d_flatbed_fmtv_usarmy",                   50],
		["RHS_M2A3_BUSKI",                                             25],
		["RHS_M2A3",                                                   25],
		["rhsusf_m1025_w_s_m2",                                        25],
		["rhsusf_m1025_d",                                             25],
		["rhsusf_M1083A1P2_B_M2_wd_fmtv_usarmy",                       50],
		["rhsusf_M1083A1P2_B_M2_d_MHQ_fmtv_usarmy",                    50],
		["rhsusf_M1083A1P2_B_M2_d_Medical_fmtv_usarmy",                50],
		["rhsusf_rg33_d",                                              25],
		["rhsusf_rg33_wd",                                             25],
		["rhs_uaz_vmf",		                         	               25],
		["rhs_uaz_open_vmf", 	                                       25],
		["rhs_uaz_MSV_01", 		                                       25],         
		["rhs_uaz_open_MSV_01",	                                       25],         
		["rhs_uaz_vdv", 			                                   25],         
		["rhs_uaz_open_vdv",	 	                                   25],         
		["rhs_uaz_vv", 			                                       25],         
		["rhs_uaz_open_vv",	 	                                       25],         
		["RHS_Ural_Open_Civ_01",                                       50],
		["RHS_Ural_Open_Civ_02",                                       50],
		["RHS_Ural_Open_Civ_03",                                       50],
		["RHS_Ural_MSV_01",                                            50],
		["RHS_Ural_VDV_01",                                            50],
		["RHS_Ural_VMF_01",                                            50],
		["RHS_Ural_VV_01",                                             50],
		["RHS_Ural_Flat_MSV_01",                                       50],
		["RHS_Ural_Flat_VDV_01",                                       50],
		["RHS_Ural_Flat_VMF_01",                                       50],
		["RHS_Ural_Flat_VV_01",                                        50],
		["RHS_Ural_Open_MSV_01",                                       50],
		["RHS_Ural_Open_VDV_01",                                       50],
		["RHS_Ural_Open_VMF_01",                                       50],
		["RHS_Ural_Open_VV_01",                                        50],
		["RHS_Ural_Open_Flat_MSV_01",                                  50],
		["RHS_Ural_Open_Flat_VDV_01",                                  50],
		["RHS_Ural_Open_Flat_VMF_01",                                  50],
		["RHS_Ural_Open_Flat_VV_01",                                   50],
		["RHS_Ural_Fuel_MSV_01",                                       25],
		["RHS_Ural_Fuel_VDV_01",                                       25],
		["RHS_Ural_Fuel_VMF_01",                                       25],
		["RHS_Ural_Fuel_VV_01",                                        25],
		["rhs_bmp3_late_msv",                                          25],
		["rhs_btr60_vmf",                                              25],
		["rhs_btr60_vdv",                                              25],
		["rhs_btr60_vv",                                               25],
		["rhs_btr60_msv",                                              25],
		["rhsusf_rg33_m2_d",                                           25],
		["rhsusf_rg33_m2_wd",                                          25],
		["rhsusf_rg33_usmc_d",                                         25],
		["rhsusf_rg33_usmc_wd",                                        25],
		["rhsusf_rg33_m2_usmc_d",                                      25],
		["rhsusf_rg33_m2_usmc_wd",                                     25],
		["rhs_typhoon_vdv",                                            100],
		["RHS_BTR70",                                                  25],
		["RHS_BTR70_MSV",                                              25],
		["RHS_BTR70_VDV",                                              25],
		["RHS_BTR70_VMF",                                              25],
		["RHS_BTR70_VV",                                               25],
		["RHS_BTR80_MSV",                                              25],
		["RHS_BTR80_VDV",                                              25],
		["RHS_BTR80_VMF",                                              25],
		["RHS_BTR80A_VDV",                                             25],
		["RHS_BTR80A_VMF",                                             25],
		["RHS_BTR80A_VV",                                              25],
		["RHS_BTR80_VV",                                               25],
		["RHS_BTR80A_MSV",                                             25],
		["rhs_t72ba_tv",                                               25],
		["rhsusf_m1a2sep1tuskiiwd_usarmy",                             25],
		["rhs_t72bc_tv",                                               25],
		["rhsusf_m1a1aimwd_usarmy",                                    25],
		["rhsusf_M977A4_REPAIR_BKIT_M2_usarmy_wd",                     100],
		["rhsusf_mrzr4_d_mud",                                         25],
		["rhsusf_M1232_usarmy_d",                                      50],
		["rhsusf_M1232_usarmy_wd",                                     50],
		["rhsusf_M1232_M2_usarmy_d",                                   50],
		["rhsusf_M1232_M2_usarmy_wd",                                  50],
		["rhsusf_M1232_MK19_usarmy_d",                                 50],
		["rhsusf_M1232_MK19_usarmy_wd",                                50],
		["rhsusf_M1237_M2_usarmy_d",                                   50],
		["rhsusf_M1237_M2_usarmy_wd",                                  50],
		["rhsusf_M1237_MK19_usarmy_d",                                 50],
		["rhsusf_M1237_MK19_usarmy_wd",                                50],
		["rhsusf_M977A4_usarmy_wd",                                    100],
		["rhsusf_M977A4_usarmy_d",                                     100],                 
		["rhsusf_M977A4_BKIT_M2_usarmy_wd",                            100],
		["rhsusf_M977A4_BKIT_M2_usarmy_d",                             100],
		["rhsusf_M977A4_AMMO_BKIT_M2_usarmy_wd",                       100],
		["rhsusf_M977A4_AMMO_BKIT_M2_usarmy_d",                        100],
		["rhsusf_M978A4_BKIT_usarmy_wd",                               100],
		["rhsusf_M978A4_BKIT_usarmy_d",                                100],
		["rhs_bmp3mera_msv",	                                       25]
			
];

R3F_LOG_CFG_can_be_transported_cargo = R3F_LOG_CFG_can_be_transported_cargo +
[
		["rhsusf_mrzr4_d_mud",                                         25]
];

R3F_LOG_CFG_can_be_moved_by_player = R3F_LOG_CFG_can_be_moved_by_player +
[
    
];